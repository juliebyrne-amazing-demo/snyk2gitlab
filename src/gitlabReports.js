const fs = require('fs');
const parseIdentifiers = require('./parser/parseIdentifiers');
const parseLinks = require('./parser/parseLinks');
const d = new Date();
const timestamp = d.toISOString();
const startTime = timestamp.split('.').slice(0, 4)[0];

//const reportObj = JSON.parse(fs.readFileSync(0, 'utf8').toString('utf8'));

function gitlabReports(reportObj) {

  const vulnerabilities = reportObj.vulnerabilities.map(v => ({
    id: v.id,
    category: 'dependency_scanning',
    name: v.title,
    message: v.title,
    description: 'PLACEHOLDER',
    // description: v.description,
    cve: 'package.lock'.
        concat(':', v.name).
        concat(':', 'snyk').
        concat(':', v.id),
    severity: v.severity.charAt(0).toUpperCase() + v.severity.slice(1),
    // TODO
    solution: 'TODO',
    scanner: {
      id: 'snyk',
      name: 'Snyk',
    },
    location: {
      file: 'package-lock.json',
      dependency: {
        package: {
          name: v.name,
        },
        version: v.version,
      },
    },
    identifiers: parseIdentifiers(v.identifiers),
    links: parseLinks(v.references),
  }));
  // eslint-disable-next-line no-unused-vars
  const result = [reportObj].map(x => ({
    version: '14.0.0',
    vulnerabilities: vulnerabilities,
    // TODO
    remediations: [],
    // remediations: [x.remediation],
    // TODO
    dependency_files: [
      {
        path: 'package-lock.json',
        package_manager: 'npm',
        dependencies: [
          {
            package: {
              name: 'ansi-regex',
            },
            version: '3.0.0',
          },
        ],
      },
    ],
    scan: {
      scanner: {
        id: 'snyk',
        name: 'Snyk',
        url: 'https://snyk.io',
        vendor: {
          name: 'Snyk',
        },
        version: '0.0.1',
      },
      type: 'dependency_scanning',
      // time isn't tracked in these files
      // also, heh.  what the shit.
      start_time: startTime,
      end_time: timestamp.split('.').slice(0, 4)[0],
      status: 'success',
    },
  }));
  const stringifyResult = JSON.stringify(result[0], null, 2);
  fs.writeFile('gl-dependency-scanning-report.json', stringifyResult, (err) => {
  if (err) throw err;
  console.log('Results transformed to file.');
  });
}
module.exports = gitlabReports;
