const parseIndentifiers = require('./parseIdentifiers');

test('validate identifier transformation', () => {
  const identifiersObj = {
    'CVE': [
      'CVE-2021-3807',
    ],
    'CWE': [
      'CWE-400',
    ],
  };
  const expectedResult = [{ 'name': 'snyk-CVE-2021-3807', 'url': 'https://snyk.io/', 'type': 'snyk', 'value': 'CVE-2021-3807' }, { 'name': 'snyk-CWE-400', 'type': 'snyk', 'url': 'https://snyk.io/', 'value': 'CWE-400' }];
  expect(parseIndentifiers(identifiersObj)).toStrictEqual(expectedResult);
});
